package ru.tsc.golovina.tm;

import ru.tsc.golovina.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        parseArgs(args);
    }

    private static void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    public static  void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (TerminalConst.ABOUT.equals(arg)) showAbout();
        if (TerminalConst.VERSION.equals(arg)) showVersion();
        if (TerminalConst.HELP.equals(arg)) showHelp();
    }

    public static void showHelp() {
        System.out.println(TerminalConst.VERSION + " - Display program version");
        System.out.println(TerminalConst.ABOUT + " - Display developer info");
        System.out.println(TerminalConst.HELP + " - Display list of terminal commands");
        System.exit(0);
    }

    public static void showVersion() {
        System.out.println("1.0.0");
        System.exit(0);
    }

    public static void showAbout() {
        System.out.println("Developer: Alla Golovina");
        System.out.println("e-mail: amedvedeva@tsconsulting.com");
        System.exit(0);
    }

}